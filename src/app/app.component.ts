import { Component, OnInit } from '@angular/core';

import { interval } from 'rxjs';
import { map, takeUntil, filter } from 'rxjs/operators';

/**
 * Simbolizando número e quantidade
 * de vezes que o mesmo foi sorteado
 */
interface ValorQuantidade {
  valor: number;
  quantidade: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  /**
   * Indica de quanto em quanto tempo
   * será feito um novo sorteio, em
   * milisegundos
   */
  intervalo = 1;

  /**
   * Quantidade inicial de ocorrências
   * desejadas
   */
  ocorrencias = 10;

  /**
   * Receberá o total de ocorrências para
   * controlar a "cor" do número
   */
  ocorrenciasBadge = 10;

  /**
   * Indica se estão ocorrendo sorteios
   * em determinado momento
   */
  simulandoNumeros = false;

  /**
   * Vetor de números que vai conter
   * os 60 números da Mega Sena
   */
  numeros: ValorQuantidade[] = [];

  ngOnInit(): void {
    /**
     * No início, deixamos
     * no estado inicial
     */
    this.zerar();
  }

  /**
   * Limpamos o vetor de números
   * e também as quantidades sorteadas
   */
  private zerar() {
    this.numeros = [];

    for (let i = 1; i <= 60; i++) {
      this.numeros.push({
        valor: i,
        quantidade: 0
      });
    }
  }

  /**
   * Geramos um número aleatório entre 1 e 60
   */
  private _gerarNumeroAleatorio() {
    return Math.max(0, Math.floor(Math.random() * 60));
  }

  /**
   * Filtramos os números cuja quantidade de sorteios
   * é maior ou igual ao desejado. Retorna true se foram
   * filtrados 6 números,
   */
  private _checkFimSimulacao(): boolean {
    const itensFiltrados = this.numeros.filter(
      item => item.quantidade >= this.ocorrencias
    );

    return itensFiltrados.length === 6;
  }

  /**
   * Preparação da execução, onde os
   * dados são zerados e há a indicação
   * de sorteio em andamento
   */
  private _prepararExecucao() {
    this.zerar();
    this.simulandoNumeros = true;
    this.ocorrenciasBadge = this.ocorrencias;
  }

  /**
   * Simulação dos sorteios
   * com setInterval (leitura
   * mais complexa)
   */
  public simularComSetInterval() {
    /**
     * Vai guardar o id do setTimeout
     * para que possa ser cancelado
     * posteriormente
     */
    let idSimulacao: any;

    /**
     * Prepara a execução, indicando
     * que ocorrerão sorteios
     */
    this._prepararExecucao();

    /**
     * Callback a ser executado a cada
     * sorteio. Nele, geramos um número
     * aleatório entre 1 e 60 e incrementamos
     * a quantidade sorteada desse número. Por
     * fim, verificamos se a simulação pode
     * ser finalizada. Em caso afirmativo, executamos
     * o clearInterval
     */
    const simulacao = () => {
      const index = this._gerarNumeroAleatorio();
      this.numeros[index].quantidade++;

      if (this._checkFimSimulacao()) {
        clearInterval(idSimulacao);
        this.simulandoNumeros = false;
      }
    };

    /**
     * Ativando a simulação com o callback acima
     */
    idSimulacao = setInterval(simulacao, this.intervalo);
  }

  /**
   * Simulação do sorteio com RxJS
   */
  public simularComRxJS() {
    /**
     * Criando e configurando Observable principal
     */
    const obs$ = interval(this.intervalo).pipe(
      map(intervalo => {
        return this._gerarNumeroAleatorio();
      })
    );

    /**
     * Observable para indicar a condição
     * de parada do Observable principal
     */
    const condicaoParada$ = obs$.pipe(
      filter(() => {
        const itensFiltrados = this.numeros.filter(
          item => item.quantidade >= this.ocorrencias
        );

        return itensFiltrados.length === 6;
      })
    );

    /**
     * Preparação da execução
     */
    this._prepararExecucao();

    /**
     * Solicitando a execução com subscribe
     */
    obs$.pipe(takeUntil(condicaoParada$)).subscribe(
      /**
       * Ação de execução a cada valor obtido (next)
       */
      index => {
        this.numeros[index].quantidade++;
      },

      /**
       * Ações em caso de ocorrência de erro (error)
       */
      erro => {
        console.log('Erro', erro);
      },

      /**
       * Ações de finalização (finish)
       */
      () => {
        this.simulandoNumeros = false;
      }
    );
  }
}
