import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-numero',
  templateUrl: './numero.component.html',
  styleUrls: ['./numero.component.css']
})
export class NumeroComponent {
  @Input()
  valor: number;

  @Input()
  quantidade: number;

  @Input()
  objetivo: number;
}
